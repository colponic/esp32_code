import json
import requests
from paho.mqtt import client as mqtt_client

broker = 'broker.emqx.io'
port = 1883
topic = 'colponic/response/#'
client_id = 'server'
username = 'dev_test'
password = 'aio_ftKW54LxSP3Yl4Uw2Q3YvCxXCE5X'


def connect_mqtt() -> mqtt_client:
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(client_id)
    client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client


def subscribe(client: mqtt_client):
    def on_message(client, userdata, msg):
        print(f"Received {msg.payload.decode()} from {msg.topic} topic")
        topic_root = msg.topic.split('/')[2]
        device_id = msg.topic.split('/')[3]
        print(topic_root)
        if topic_root == 'actuator':
            message = msg.payload.decode().split('_')
            res = requests.put(
                url=f'https://www.api-colponic.tk/devices/update_actuator_state/{device_id}',
                json={
                    'name': message[0],
                    'state': bool(int(message[1]))
                }
            )
        elif topic_root == 'sensor':
            message = json.loads(msg.payload.decode())
            res = requests.post(url=f'https://www.api-colponic.tk/devices/save_data_sensor/{device_id}', json=message)
        else:
            res = 'STATUS'
        print(res)
    client.subscribe(topic)
    client.on_message = on_message


def run():
    client = connect_mqtt()
    subscribe(client)
    client.loop_forever()


if __name__ == '__main__':
    run()
