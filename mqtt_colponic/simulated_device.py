import random
import json
from paho.mqtt import client as mqtt_client

def connect_mqtt() -> mqtt_client:
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(client_id)
    client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client


def subscribe(client: mqtt_client):
    def on_message(client, userdata, msg):
        message = msg.payload.decode()
        print(f'Received {msg.payload.decode()} from {msg.topic} topic')
        if msg.topic == root_topic_subscribe:
            if message == 'status':
                client.publish(root_topic_publish_status, 'connected')
            elif message == 'read_sensors':
                data = {
                    'temp-0': round(random.uniform(0, 30), 2),
                    'pressure': round(random.uniform(10000, 80000), 2),
                    'elevation': round(random.uniform(2000, 3000), 2),
                    'lux': round(random.uniform(0, 4500), 2),
                    'temp-1': round(random.uniform(0, 30), 2)
                }
                print(json.dumps(data))
                client.publish(root_topic_publish_sensor, json.dumps(data))
            elif message == 'actuator1_on':
                client.publish(root_topic_publish_actuator, "motor_1")
            elif message == 'actuator1_off':
                client.publish(root_topic_publish_actuator, "motor_0")
            elif message == 'actuator2_on':
                client.publish(root_topic_publish_actuator, "lamp_1")
            elif message == 'actuator2_off':
                client.publish(root_topic_publish_actuator, "lamp_0")
    client.subscribe(root_topic_subscribe)
    client.on_message = on_message


def run():
    client = connect_mqtt()
    subscribe(client)
    client.loop_forever()


if __name__ == '__main__':
    client_id = input('Device ID:')
    broker = 'broker.emqx.io'
    port = 1883
    mqtt_pass = "aio_ftKW54LxSP3Yl4Uw2Q3YvCxXCE5X"
    root_topic_subscribe = f'colponic/request/{client_id}'
    root_topic_publish_actuator = f'colponic/response/actuator/{client_id}'
    root_topic_publish_sensor = f'colponic/response/sensor/{client_id}'
    root_topic_publish_status = f'colponic/response/status/{client_id}'
    username = 'dev_test'
    password = 'aio_ftKW54LxSP3Yl4Uw2Q3YvCxXCE5X'
    run()

