from paho.mqtt import client as mqtt_client
from time import sleep

broker = 'broker.emqx.io'
port = 1883
client_id = 'generator'
username = 'dev_test'
password = 'aio_ftKW54LxSP3Yl4Uw2Q3YvCxXCE5X'


def connect_mqtt():
	def on_connect(client, userdata, flags, rc):
		if rc == 0:
			print("Connected to MQTT Broker!")
		else:
			print("Failed to connect, return code %d\n", rc)

	client = mqtt_client.Client(client_id)
	client.username_pw_set(username, password)
	client.on_connect = on_connect
	client.connect(broker, port)
	return client


def publish_message(msg):
	client.publish(topic, msg)


if __name__ == '__main__':
	client = connect_mqtt()
	client.loop_start()
	print('Iniciando...')
	sleep(1)
	while True:
		b = input('Command and client id: ')
		if b == 'q':
			break
		a = b.split('.')[0]
		client_id = b.split('.')[1]
		topic = f'colponic/request/{client_id}'
		if a == 'r':
			publish_message('read_sensors')
		elif a == 's':
			publish_message('status')
		elif a == 'i':
			publish_message('actuator1_on')
		elif a == 'k':
			publish_message('actuator1_off')
		elif a == 'o':
			publish_message('actuator2_on')
		elif a == 'l':
			publish_message('actuator2_off')
