## Bienvenido al repositorio Esp32 code 
En este repositorio estarán disponibles los códigos usados en este proyecto.
### Instrucciones:

**Software**

Antes de clonar los algoritmos de prueba, debe instalar  Visual Studio conde y así mismo Pyton  (3.9.1),  así mismo para usar el algoritmo del módulo  se recomienda usar la ID de Arduino (1.8.16).

**librerias**

A continuación se nombran las librerías de Arduino que usaron en el proyecto:

- AceWire 0.3.2 
- PubSubClient 1.11.1 
- BH1750 1.2.0
- ArduinoJson 6.18.5
- Adafruit BME280 2.2.1
- OneWire 2.3.5 
- DallasTemperature 3.9.0

**Diagrama de flujo modulo**

![alt text](./images/modulo.png)
