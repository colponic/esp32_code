import time
import random

from paho.mqtt import client as mqtt_client


broker = 'broker.emqx.io'
port = 1883
topic =  'colponic/actuator/50'
# generate client ID with pub prefix randomly
client_id = f'python-mqtt-{random.randint(0, 100)}'
username = 'dev_test'
password = 'aio_ftKW54LxSP3Yl4Uw2Q3YvCxXCE5X'



def connect_mqtt():
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(client_id)
    client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client


def publish(client):
    msg_count = 0
    state = False
    while True:
        time.sleep(5)
        result = client.publish(topic, 'read_sesnors')
        print('...OK')
       


def run():
    client = connect_mqtt()
    client.loop_start()
    publish(client)

if __name__ == '__main__':
    run()