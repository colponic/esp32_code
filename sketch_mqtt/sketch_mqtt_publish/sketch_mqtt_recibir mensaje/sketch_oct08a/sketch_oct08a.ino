//**************************************
//*********** librerias **************
//**************************************
#include <WiFi.h>
#include <Wire.h> // libreria I2C
#include <PubSubClient.h>
#include <BH1750.h>
#include <ArduinoJson.h>
#include <Adafruit_BMP280.h>
#include <OneWire.h>
#include <DallasTemperature.h>

//**************************************
//*********** MQTT CONFIG **************
//**************************************

const char *mqtt_server = "ioticos.org";
const int mqtt_port =1883 ;
const char *mqtt_user = "U20QXmwjCS9W4vP";
const char *mqtt_pass = "JMhag9vHJrvoCkD";
const char *root_topic_subscribe = "veGv4DSBXuu6qzB/input";
const char *root_topic_publish = "veGv4DSBXuu6qzB/output";
const String clientID ="Esp32jaime";
//**************************************
//*********** WIFICONFIG ***************
//**************************************
const char* ssid = "red steven";
const char* password =  "85659720";
//**************************************
//*********** GLOBALES   ***************
//**************************************
WiFiClient espClient;
PubSubClient client(espClient);
//char msg[25];
//long count=0;
unsigned long ultimaConsulta = 0;
unsigned long tiempoConsulta = 5000;


//**************************************
//*********** parametros ***************
//**************************************

BH1750 lightMeter;

OneWire ourWire(4); //Se establece el pin 4 del ESP32 para la lectura del sensor
DallasTemperature DS18B20(&ourWire);
Adafruit_BMP280 bmp;

//************************
//** F U N C I O N E S ***
//************************
void reconnect();
void setup_wifi();
void temporizador();

void setup() {
  Serial.begin(115200);
  setup_wifi();
  
  Wire.begin(); // configura el esp32 como maestro

  lightMeter.begin(); //direccion 12C 0x23 Se inicia el sensor  BH1750

  DS18B20.begin(); //Se inicia el sensor  DS18B20 
  bmp.begin(0x76);
  bmp.setSampling(Adafruit_BMP280::MODE_NORMAL,     /* Operating Mode. */
                  Adafruit_BMP280::SAMPLING_X2,     /* Temp. oversampling */
                  Adafruit_BMP280::SAMPLING_X16,    /* Pressure oversampling */
                  Adafruit_BMP280::FILTER_X16,      /* Filtering. */
                  Adafruit_BMP280::STANDBY_MS_500); /* Standby time. */
   //Se obtiene la temperatura en ºC
  
  client.setServer(mqtt_server, mqtt_port); 
}
void loop() {
    
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
   // Temporizador
  temporizador();
}

void censores()
  {
//----------------------------SENSOR_LUMINOCIDAD-----------------------------------
    float lux = lightMeter.readLightLevel();
    char clux[7]; // Número de caracteres máximo 6 (XXX.XX)
    snprintf(clux, 7, "%.0f", lux); // Convertir un float en un array de caracteres
//----------------------------SENSOR_TEMPERATURA_DE_AGUA-----------------------------
    DS18B20.requestTemperatures();
    float tem2= DS18B20.getTempCByIndex(0);
    char ctem2 [7]; // Número de caracteres máximo 6 (XXX.XX)
    snprintf(ctem2,7,"%.0f",tem2);
//----------------------------SENSOR_BMP280-------------------------------------------
    float tem1=bmp.readTemperature();
    char ctem1 [7]; 
    snprintf(ctem1,7,"%.0f",tem1); 

    float alt=bmp.readAltitude(1013.25);
    char calt [7]; 
    snprintf(calt,7,"%.0f",alt); 

    float pre = bmp.readPressure();
    char cpre [7]; 
    snprintf(cpre,7,"%.0f" ,pre);
//----------------------------generacion json--------------------------------------
   char output[128];
  StaticJsonDocument<128> doc;

doc["Temperatura1"] = ctem1,"c";
doc["Precion"] = cpre;
doc["altitud"] = calt;
doc["luminocidad"] = clux;
doc["Temperatura"] = ctem2;

    serializeJson(doc, output);
    client.publish(root_topic_publish,output);
    Serial.println(output);
   
    
  }

 void temporizador() {
  // Comprobar si se ha dado la vuelta
  if (millis() < ultimaConsulta) {
    // Asignar un nuevo valor
    ultimaConsulta = millis();
  }
 
  if ((millis() - ultimaConsulta) > tiempoConsulta) {
    // Marca de tiempo
    ultimaConsulta = millis();
    // Llamada a la función para obtener los datos y actualziar pantalla Nextion
    censores();
  }
}
//*****************************
//***    CONEXION WIFI      ***
//*****************************
void setup_wifi(){
  delay(10);
  // Nos conectamos a nuestra red Wifi
  Serial.println();
  Serial.print("Conectando a ssid: ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("Conectado a red WiFi!");
  Serial.println("Dirección IP: ");
  Serial.println(WiFi.localIP());
}
//*****************************
//***    CONEXION MQTT      ***
//*****************************

void reconnect() {

  while (!client.connected()) {
    Serial.print("Intentando conexión Mqtt...");
    // Creamos un cliente ID
    String clientId = "IOTICOS_H_W_";
    clientId += String(random(0xffff), HEX);
    // Intentamos conectar
    if (client.connect(clientId.c_str(),mqtt_user,mqtt_pass)) {
      Serial.println("Conectado!");
      // Nos suscribimos
      if(client.subscribe(root_topic_subscribe)){
        Serial.println("Suscripcion ok");
      }else{
        Serial.println("fallo Suscripciión");
      }
    } else {
      Serial.print("falló :( con error -> ");
      Serial.print(client.state());
      Serial.println(" Intentamos de nuevo en 5 segundos");
      delay(5000);
    }
  }
}
