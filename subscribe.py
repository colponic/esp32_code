import random
import json
from datetime import datetime

from paho.mqtt import client as mqtt_client


broker = 'broker.emqx.io'
port = 1883
topic =  'dev_test/group/rkc6zW5Bw6NrmhK'
# generate client ID with pub prefix randomly
client_id = f'python-mqtt-{random.randint(0, 100)}'
username = 'dev_test'
password = 'aio_ftKW54LxSP3Yl4Uw2Q3YvCxXCE5X'



def connect_mqtt() -> mqtt_client:
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print(f"Connected to MQTT Broker {topic}")
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(client_id)
    client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client


def subscribe(client: mqtt_client):
    def on_message(client, userdata, msg):
        json_msg = json.loads(msg.payload.decode())
        for key in json_msg:
            print (key, ' : ', json_msg[key])
        print(f"from {msg.topic} topic")
        now = datetime.now()
        current_time = now.strftime("%H:%M:%S")
        print("Current Time =", current_time)

    client.subscribe(topic)
    client.on_message = on_message


def run():
    client = connect_mqtt()
    subscribe(client)
    client.loop_forever()

if __name__ == '__main__':
    run()
