#include <Wire.h> // libreria I2C
#include <ArduinoJson.h>
//----------------------- sensor BMP280------------------//
#include <Adafruit_BMP280.h>

Adafruit_BMP280 bmp;
//------------------------sensor BH1750-------------------//
#include <BH1750.h>
BH1750 lightMeter;
//-----------------------sensor DS18B20-------------------
#include <OneWire.h>
#include <DallasTemperature.h>
OneWire ourWire(15); //Se establece el pin 4 del ESP32 para la lectura del sensor
DallasTemperature DS18B20(&ourWire); //Se declara una variable u objeto para el sensor

  
void setup() {
  Serial.begin(115200);
  Wire.begin(); // configura el esp32 como maestro
  //----- i
  lightMeter.begin(); //direccion 12C 0x23 Se inicia el sensor  BH1750
  DS18B20.begin(); //Se inicia el sensor  DS18B20
  //------------------inicia el sensro bmp280-------------------
  bmp.begin(0x76);
  bmp.setSampling(Adafruit_BMP280::MODE_NORMAL,     /* Operating Mode. */
                  Adafruit_BMP280::SAMPLING_X2,     /* Temp. oversampling */
                  Adafruit_BMP280::SAMPLING_X16,    /* Pressure oversampling */
                  Adafruit_BMP280::FILTER_X16,      /* Filtering. */
                  Adafruit_BMP280::STANDBY_MS_500); /* Standby time. */
   //Se obtiene la temperatura en ºC

}
void loop() 
{

float lux = lightMeter.readLightLevel();
char clux [7]; // Número de caracteres máximo 6 (XXX.XX)
snprintf(clux, 7, "%.0f", lux);

DS18B20.requestTemperatures(); //Se envía el comando para leer la temperatura 
float tem2= DS18B20.getTempCByIndex(0);
char ctem2 [7]; // Número de caracteres máximo 6 (XXX.XX)
snprintf(ctem2,7,"%.0f",tem2);

float tem1=bmp.readTemperature();
char ctem1 [7]; // Número de caracteres máximo 6 (XXX.XX)
snprintf(ctem1,7,"%.0f",tem1); 

float alt=bmp.readAltitude(1013.25);
char calt [7]; // Número de caracteres máximo 6 (XXX.XX)
snprintf(calt,7,"%.0f",alt); 

float pre = bmp.readPressure();
char cpre [7]; // Número de caracteres máximo 6 (XXX.XX)
snprintf(cpre,7,"%.0f" ,pre); 

   
char output[128];
  StaticJsonDocument<128> doc;

doc["Temperatura1"] = ctem1,"c";
doc["Precion"] = cpre;
doc["altitud"] = calt;
doc["luminocidad"] = clux;
doc["Temperatura"] = ctem2;

    serializeJson(doc, output);
  Serial.println(output);
  
    
        delay(2000);
}
 
 /*   Serial.print(F("Temperature: "));
    Serial.print(tem1);
    Serial.println(" *C");

    Serial.print(F("Pressure: "));
    Serial.print(pre);
    Serial.println(" Pa");

    Serial.print(F("Approx altitude: "));
    Serial.print(alt); 
    Serial.println(" m");

    Serial.print("Light: ");
    Serial.print(lux);
    Serial.println(" lx");

    Serial.print("Temperature_water: ");
    Serial.print(tem2);
    Serial.println(" °C");
*/
